
import java.util.ArrayList;



/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ondrejtybl
 */
public class Game {
    
    public static ArrayList<Value> list = new ArrayList<>(); //informace o tazích
    private Value first = Value.FIRST;                      // pomocné privátní values pro výpočet checkWin
    private Value second = Value.SECOND;
    private Value none = Value.NONE;
    
    public void setValue(int x, Value value) {
        list.set(x,value);
    }
    
    public Value getValue(int x) {
        return list.get(x);
    }
    
    public Value checkWin() {
        return none;
    }
    
}
