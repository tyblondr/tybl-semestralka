/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ondrejtybl
 */
public enum Value {
    
    //konstanty určující, kdo zaškrtl políčko
    
    FIRST(1), SECOND(2), NONE(0);
    
    private final int num;
    
    private Value(int num) {
        this.num = num;
    }
    
    public int getNum() {
        return this.num;
    }
    
    
    
    
}
